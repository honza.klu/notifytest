import logging
import json, os

from flask import request, Response, render_template, jsonify, Flask
from pywebpush import webpush, WebPushException

app = Flask(__name__)
app.config['SECRET_KEY'] = '9OLWxND4o83j4K4iuopO'

data_path = os.getenv('DATA_PATH', "./data/")
http_port = int(os.getenv('HTTP_PORT', "8080"))

DER_BASE64_ENCODED_PRIVATE_KEY_FILE_PATH = os.path.join(
    os.getcwd(),
    os.path.join(data_path, "private_key.txt"),
)
DER_BASE64_ENCODED_PUBLIC_KEY_FILE_PATH = os.path.join(
    os.getcwd(),
    os.path.join(data_path, "public_key.txt"),
)

VAPID_PRIVATE_KEY = open(DER_BASE64_ENCODED_PRIVATE_KEY_FILE_PATH, "r+").readline().strip("\n")
VAPID_PUBLIC_KEY = open(DER_BASE64_ENCODED_PUBLIC_KEY_FILE_PATH, "r+").read().strip("\n")

VAPID_CLAIMS = {
"sub": "mailto:honza.klu@gmail.com"
}

@app.route('/')
def index():
    return render_template('index.html')

@app.route("/api/get_vapid_public_key", methods=["GET", "POST"])
def get_vapid_public_key():
    return Response(response=json.dumps({"public_key": VAPID_PUBLIC_KEY}),
                    headers={"Access-Control-Allow-Origin": "*"}, content_type="application/json")

@app.route("/api/send_subscription", methods=["GET", "POST"])
def send_subscription():
    print(str(request.json))
    return Response(response=json.dumps({}),
                    content_type="application/json")

@app.route("/subscription/", methods=["GET", "POST"])
def subscription():
    """
        POST creates a subscription
        GET returns vapid public key which clients uses to send around push notification
    """

    if request.method == "GET":
        return Response(response=json.dumps({"public_key": VAPID_PUBLIC_KEY}),
            headers={"Access-Control-Allow-Origin": "*"}, content_type="application/json")

    subscription_token = request.get_json("subscription_token")
    return Response(status=201, mimetype="application/json")

if __name__ == "__main__":
    app.run(host="0.0.0.0",port=http_port)