FROM debian:latest

RUN apt-get update
RUN apt-get install -y python3 python3-venv python3-pip
RUN pip3 install flask

EXPOSE 8080

WORKDIR /root/
ENTRYPOINT ./notifytest_src/scripts/start.sh

COPY . /root/notifytest_src
RUN pip3 install ./notifytest_src
RUN chmod -R +x ./notifytest_src

