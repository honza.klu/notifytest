#!/bin/sh

if [ ! -f ./data ]; then
    mkdir data
fi
if [ ! -f ./data/vapid_private.pem ]; then
    openssl ecparam -name prime256v1 -genkey -noout -out ./data/vapid_private.pem
fi

openssl ec -in ./data/vapid_private.pem -outform DER|tail -c +8|head -c 32|base64|tr -d '=' |tr '/+' '_-' > ./data/private_key.txt
openssl ec -in ./data/vapid_private.pem -pubout -outform DER|tail -c 65|base64|tr -d '=' |tr '/+' '_-' > ./data/public_key.txt