from setuptools import setup

setup(
    name='notifytest',
    version='0.3.1',
    description='Test of web push notifications.',
    author='ERA erao',
    author_email='{j.klusacek}@era.aero',
    packages=['notifytest'],
    include_package_data=True,
    python_requires='>=3.6',
    install_requires=[
        "flask",
        "pywebpush",
    ],
)